package common

import (
	"io"
	"net/http"
	"os"
)

type httpCommon struct {
}

var Http httpCommon

func (*httpCommon) DownLoad(url string, savePath string) {
	// Get the data
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	// Create output file
	out, err := os.Create(savePath)
	if err != nil {
		panic(err)
	}
	defer out.Close()
	// copy stream
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		panic(err)
	}

}
