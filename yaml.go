package common

import (
	"gopkg.in/yaml.v3"
	"io/ioutil"
	"log"
)

func GetYAMlContent(path string) (error, map[string]interface{}) {
	//path, _ = filepath.Abs(path)
	var result map[string]interface{}
	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatalf("解析yaml读取错误: %v", err)
		return err, result
	}
	//fmt.Println(string(content))
	//fmt.Printf("init data: %v", config)
	err = yaml.Unmarshal(content, &result)
	if err != nil {
		log.Fatalf("解析yaml出错: %v", err)
		return err, result
	}
	//fmt.Printf("File config: %v", result)
	return nil, result
}
