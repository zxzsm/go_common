package common

import (
	"io"
	"os"
	"path/filepath"
	"sync"
)

type file struct {
}

var File file
var mutex sync.RWMutex

func (file) Exist(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil || os.IsExist(err)
}

func (f file) Write(fileName string, writeString string) error {
	var file *os.File
	var err error
	defer file.Close()
	defer mutex.Unlock()
	mutex.Lock()
	dir := filepath.Dir(fileName)
	_, err = filepath.EvalSymlinks(dir)
	if err != nil {
		err = os.MkdirAll(dir, 0777)
	}
	if err != nil {
		return err

	}
	file, err = os.OpenFile(fileName, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}
	_, err = io.WriteString(file, writeString) //写入文件(字符串)

	return err
}
